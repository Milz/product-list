const btnSubmit = document.querySelector('button[type=submit]');
const gallery = document.querySelector('.gallery');
const inputs = document.querySelectorAll('form input');
const inputImg = document.querySelector('input[type=file]');
const inputName = document.querySelector('input[name=name]');
const inputDescription = document.querySelector('input[name=description]');
const inputPrice = document.querySelector('input[name=price]');
const cards = JSON.parse(localStorage.getItem('cards')) || [];
let src = '';

function validation(price, name, description, img) {
  let reg = /^\d{0,3}[.,]?\d{1,2}$/;
  price.classList.remove('is-invalid');
  name.classList.remove('is-invalid');
  description.classList.remove('is-invalid');

  if (img.value === '') {
    alert('Select the image');
  } else if (img.files[0].size > 1048576) {
    alert('The image should be less than 1mb');
  } else if (price.value.match(reg) === null) {
    price.classList.add('is-invalid');
  } else if (name.value.length < 3) {
    name.classList.add('is-invalid');
  } else if (description.value.length < 3) {
    description.classList.add('is-invalid');
  } else {
    return true;
  }
}

function readURL() {
  if (this.files && this.files[0]) {
    let reader = new FileReader();
    reader.addEventListener('load', () => {
      src = reader.result;
    });
    reader.readAsDataURL(this.files[0]);
  }
}

inputImg.addEventListener('change', readURL);

function addCard() {
  const name = inputName.value;
  const description = inputDescription.value;
  const price = inputPrice.value;
  const card = {
    image: src,
    name,
    description,
    price
  };
  cards.push(card);
  printCards(cards, gallery);
  localStorage.setItem('cards', JSON.stringify(cards));
  inputs.forEach(input => input.value = '');
}

function deleteCard(e) {
  const id = e.target.parentElement.parentElement.dataset.id;
  cards.splice(id, 1);
  printCards(cards, gallery);
  localStorage.setItem('cards', JSON.stringify(cards));
}

function editCard(e) {
  const card = e.target.parentElement.parentElement;
  const id = card.dataset.id;
  card.innerHTML = `<form class="col-12">
        <img src="${cards[id].image}" class="card-img-top" alt="product">
        <div class="form-group">
          <label for="formControlFile">Image of the product</label>
          <input type="file" class="form-control-file" id="formControlFile">
        </div>
        <div class="form-group">
          <input type="text"
           class="form-control"
           name="name"
           value=${cards[id].name}
           placeholder="Product name"
           required
           >
        </div>
        <div class="form-group">
          <input type="text" 
          class="form-control"
          name="description"
          value=${cards[id].description}
          placeholder="Product description"
          required
          >
        </div>
        <div class="form-group">
          <input type="text"
           class="form-control"
           name="price"
           value=${cards[id].price}
           placeholder="Price in uah"
           required
          >
        </div>
        <button type="submit" class="btn btn-success save">Save</button>
      </form>`;

  const inputImg = card.querySelector('input[type=file]');
  inputImg.addEventListener('change', readURL);

  const btnSave = card.querySelector('button.save');
  const inputName = card.querySelector('input[name=name]');
  const inputDescription = card.querySelector('input[name=description]');
  const inputPrice = card.querySelector('input[name=price]');

  btnSave.addEventListener('click', (e) => {
    e.preventDefault();
    if (validation(inputPrice, inputName, inputDescription, inputImg)) {
      saveCard(card, id);
    }
  })
}

function saveCard(card, id) {
  const inputImg = card.querySelector('input[type=file]');
  let newSrc = '';
  if (inputImg.value.length > 0) {
    readURL.bind(inputImg);
    newSrc = src;
  } else {
    newSrc = card.querySelector('img').src;
  }
  const name = card.querySelector('input[name=name]').value;
  const description = card.querySelector('input[name=description]').value;
  const price = card.querySelector('input[name=price]').value;
  cards[id] = {
    image: newSrc,
    name,
    description,
    price
  };
  printCards(cards, gallery);
  localStorage.setItem('cards', JSON.stringify(cards));
}

function printCards(items, itemsList) {
  itemsList.innerHTML = items.map((item, i) => {
    return `
  <div class="card col-lg-3 col-md-5 col-10" data-id=${i}>
  <img src="${item.image}" class="card-img-top" alt="producth">
    <div class="card-body">
      <h5 class="card-title">${item.name}</h5>
      <p class="card-text">${item.description}</p>
      <h6 class="card-subtitle mb-2 text-muted">${item.price} uah</h6>
      <a href="#" class="btn btn-primary delete">Delete</a>
      <a href="#" class="btn btn-primary edit">Edit</a>
    </div>
  </div>
      `;
  }).join('');

  let btnsDelete = itemsList.querySelectorAll('a.delete');
  btnsDelete.forEach(btn => btn.addEventListener('click', deleteCard));

  let btnsEdit = itemsList.querySelectorAll('a.edit');
  btnsEdit.forEach(btn => btn.addEventListener('click', editCard));
}

btnSubmit.addEventListener('click', (e) => {
  e.preventDefault();
  if (validation(inputPrice, inputName, inputDescription, inputImg)) {
    addCard();
  }
});

printCards(cards, gallery);
