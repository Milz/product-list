## Dependencies

- npm

## How to install

* Run `npm i`
* Run `npm run build`
* Open `index.html` in the root folder.

#### Restrictions

*The number of cards is limited by localStorage.*